<?php

namespace Calculator;

use Calculator\CalculatorInterface;

class StandardPHPCalculator implements CalculatorInterface
{
   /**
     * Performs the calculation itself
     *
     * @param mixed $data Data to apply the process
     * @return mixed $result Result of the process
     */
   public function performCalculation($data)
    {
        $result = array_count_values(str_word_count($data, 1));

        return $result;
    }
}
