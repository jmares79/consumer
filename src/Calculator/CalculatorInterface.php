<?php

namespace Calculator;

interface CalculatorInterface
{
    public function performCalculation($data);
}
