<?php

namespace Handler;

use Stream\StreamHandlerInterface;

class UrlHandler implements StreamHandlerInterface
{
    protected $stream;
    protected $curlResource;
    protected $data;

    public function __construct($stream)
    {
        $this->stream = $stream;
        $this->initResource();
    }

    public function setStream($stream){
        $this->stream = $stream;
    }

    public function getStream(){
        return $this->stream;
    }

    /**
     * Fetch data from the data source
     *
     * @param mixed $curlResource Resource for handling the request
     *
     * @return mixed $data To save temporarily and use the returned data
     */
    public function fetch()
    {
        $this->data = curl_exec($this->curlResource);
        curl_close($this->curlResource);

        return $this->data;
    }

    /**
     * Inits the request handling the data source
     *
     * @param boolean $https For checking whether HTTPS protocol is used or not
     *
     * @return void $curlResource Sets the internal resource
     */
    protected function initResource($https = true)
    {
        $this->curlResource = curl_init();

        curl_setopt($this->curlResource, CURLOPT_URL, $this->stream);
        curl_setopt($this->curlResource, CURLOPT_RETURNTRANSFER, 1);

        if ($https) { $this->disableSslVerify(); }
    }

    /**
     * Disables the SSL verifying process during cURL.
     *
     * @param void $curlResource Internal resource
     */
    protected function disableSslVerify()
    {
        curl_setopt($this->curlResource, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->curlResource, CURLOPT_SSL_VERIFYPEER, 0);
    }
}
