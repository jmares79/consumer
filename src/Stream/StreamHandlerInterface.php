<?php

namespace Stream;

interface StreamHandlerInterface
{
    public function setStream($stream);
    public function getStream();
    // public function read();
    public function fetch();
}
