<?php

namespace Filter;

use Filter\FilterInterface;

class StandardFilter implements FilterInterface
{
    const STD_FILTER_MATCH = "/[^a-zA-Z0-9]/";
    const STD_FILTER_REPLACE = " ";
    const MAX_ELEMENTS = 100;

    /**
      * Applies the input filters
      *
      * @param string $content Data to apply the filter in
      * @return string $filteredContent The filtered data
      */
    public function inputFilter($content)
    {
        $content = strtolower($content);
        $filteredContent = preg_replace(self::STD_FILTER_MATCH, self::STD_FILTER_REPLACE, $content);

        return $filteredContent;
    }

    /**
      * Applies the output filters
      *
      * @param string $result Final result of the calculator/process
      * @return mixed The content filtered after processing
      */
    public function outputFilter($result)
    {
        arsort($result);

        return array_slice($result, 0, self::MAX_ELEMENTS);
    }
}
