<?php

namespace Filter;

interface FilterInterface
{
    public function inputFilter($content);
    public function outputFilter($content);
}
