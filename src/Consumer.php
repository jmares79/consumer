<?php

use Stream\StreamHandlerInterface;
use Calculator\CalculatorInterface;
use Filter\FilterInterface;

class Consumer
{
    protected $handler;
    protected $calculator;
    protected $filter;
    protected $content;
    protected $results;

    public function __construct(
        StreamHandlerInterface $handler,
        CalculatorInterface $calculator,
        FilterInterface $filter)
    {
        $this->handler = $handler;
        $this->calculator = $calculator;
        $this->filter = $filter;
    }

    public function getContent()
    {
        $this->content = $this->handler->fetch();
    }

    /**
      * Executes the Consumer command
      *
      * @param string $myArgument With a *description* of this argument, these may also
      *    span multiple lines.
      *
      * @return void
      */
    public function execute()
    {
        $this->run();
        $this->show();
    }

    /**
      * Run the neccesary steps for performing the processing, that is, input & output filters and calculation itself
      *
      * @param string $content The content fetched by the handler
      *
      * @return $result The result with the calculation done
      */
    protected function run()
    {
        $this->getContent();

        $filteredContent = $this->filter->inputFilter($this->content);

        $this->result = $this->calculator->performCalculation($filteredContent);

        $this->result = $this->filter->outputFilter($this->result);
    }

    /**
      * Shows the result in stdout.
      *
      * @todo Create an Output processing class
      *
      * @param string $result The result of the calculation
      * @return void Show the result via stdout
      */
    protected function show()
    {
        foreach ($this->result as $word => $count) {
            echo $word.','.$count."\n";
        }
    }
}
