<?php

require_once __DIR__.'/vendor/autoload.php';

$resources = parse_ini_file("config/config.ini");
$stream = $resources['url'];

//Entry point for the command, here we can check different parameters for selection of strategies. Out of the scope
// var_dump($argv);
// var_dump($argc);

//If no arguments, we default for the UrlHandler, StandardFilter & create the calculator
$handler = new \Handler\UrlHandler($stream);
$calculator = new \Calculator\StandardPHPCalculator();
$filter = new \Filter\StandardFilter();

$consumer = new Consumer($handler, $calculator, $filter);
$consumer->execute();
