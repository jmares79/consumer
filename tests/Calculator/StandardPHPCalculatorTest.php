<?php

use PHPUnit\Framework\TestCase;

class StandardPHPCalculatorTest extends TestCase
{
    protected $calculator;

    public function setUp()
    {
        $this->calculator = new \Calculator\StandardPHPCalculator();
    }

    /**
     * @dataProvider calculatorData
     */
    public function testStandardCalculator($string, $expected)
    {
        $actual = $this->calculator->performCalculation($string);

        $this->assertEquals($expected, $actual);
    }

    public function calculatorData()
    {
        return array(
            array(
                "Based on your input, get a random alpha numeric string random.",
                array(
                    "Based" => 1,
                    "on" => 1,
                    "your" => 1,
                    "input" => 1,
                    "get" => 1,
                    "a" => 1,
                    "random" => 2,
                    "alpha" => 1,
                    "numeric" => 1,
                    "string" => 1
                )
            )
        );
    }
}
